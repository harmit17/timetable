package com.school.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.school.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();
	
		try {	
			//	create student object
			Student tempStudent1 = new Student("Harmit", "Chauhan", "16ce012@charusat.edu.in");
			Student tempStudent2 = new Student("XYZ", "ABC", "123@charusat.edu.in");

			
			//	begin transaction
			session.beginTransaction();
			
			//	save student object 
			session.save(tempStudent1);
			session.save(tempStudent2);
			
			//	commit	transaction
			session.getTransaction().commit();
		}
		finally {
			factory.close();
		}
		
	}

}
